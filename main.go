package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/PatrickDomnick/unifi-exporter/pkg/collector"
	"gitlab.com/PatrickDomnick/unifi-exporter/pkg/unifi"
	"golang.org/x/time/rate"
)

func main() {
	// Read Config
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Warn("Config File not found")
		} else {
			log.Fatal(fmt.Errorf("fatal error config file: %w", err))
		}
	}

	// Configure Logger
	log.SetReportCaller(true)
	viper.SetDefault("logLevel", "warn")
	logLevel := viper.Get("logLevel")
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	}
	log.Println(log.IsLevelEnabled(log.DebugLevel))

	// Configure RateLimited HTTP Client
	viper.SetDefault("rateLimiter", 1)
	rateLimiter := viper.GetInt("rateLimiter")
	rl := rate.NewLimiter(rate.Every(1*time.Second), rateLimiter) // 1 request every 1 seconds
	hostIp := fmt.Sprintf("https://%s/api", viper.GetString("HostIp"))
	// Initalize Client and perform Login
	username := viper.GetString("username")
	password := viper.GetString("password")
	unifi.NewClient(rl, hostIp, username, password)

	// Create a new instance of the collector and register it with the prometheus client.
	log.Info("Starting the Collector")
	collectorDevices := collector.NewUnifiDevice()
	collectorClients := collector.NewUnifiClient()

	// Collect from all Sources
	prometheus.MustRegister(collectorDevices, collectorClients)

	// This section will start the HTTP server and expose any metrics on the /metrics endpoint.
	http.Handle("/metrics", promhttp.Handler())
	log.Info("Beginning to serve on port :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
