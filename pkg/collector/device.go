package collector

import (
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/unifi-exporter/pkg/unifi"
)

type UnifiDevice struct {
	Internet     *prometheus.Desc
	Connected    *prometheus.Desc
	Provisioned  *prometheus.Desc
	Uplink       *prometheus.Desc
	LastSeen     *prometheus.Desc
	Memory       *prometheus.Desc
	Cpu          *prometheus.Desc
	Uptime       *prometheus.Desc
	Satisfaction *prometheus.Desc
	Download     *prometheus.Desc
	Upload       *prometheus.Desc
}

func NewUnifiDevice() *UnifiDevice {
	return &UnifiDevice{
		Internet:     prometheus.NewDesc(deviceInternet, "Is the device connected to the internet", deviceLabels, nil),
		Connected:    prometheus.NewDesc(deviceConnected, "When was device connected at", deviceLabels, nil),
		Provisioned:  prometheus.NewDesc(deviceProvisioned, "When was device provisioned at", deviceLabels, nil),
		Uplink:       prometheus.NewDesc(deviceUplink, "Is there a stable uplink", deviceLabels, nil),
		LastSeen:     prometheus.NewDesc(deviceLastSeen, "When was the device last seen", deviceLabels, nil),
		Memory:       prometheus.NewDesc(deviceMemory, "Memory percentage", deviceLabels, nil),
		Cpu:          prometheus.NewDesc(deviceCpu, "Cpu percentage", deviceLabels, nil),
		Uptime:       prometheus.NewDesc(deviceUptime, "Uptime in seconds", deviceLabels, nil),
		Satisfaction: prometheus.NewDesc(deviceSatisfaction, "Device to Client Satisfaction", deviceLabels, nil),
		Download:     prometheus.NewDesc(deviceDownload, "Total Download in Bytes", deviceLabels, nil),
		Upload:       prometheus.NewDesc(deviceUpload, "Total Upload in Bytes", deviceLabels, nil),
	}
}

func (s *UnifiDevice) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *UnifiDevice) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for Unifi Devices")
	unifiDevices := unifi.GetDevices()
	for _, ud := range unifiDevices.Data {
		// Modify Boolean Values
		internet := -1
		if ud.Internet {
			internet = 1
		} else if !ud.Internet {
			internet = 0
		}
		uplink := -1
		if ud.Uplink.Up {
			uplink = 1
		} else if !ud.Uplink.Up {
			uplink = 0
		}
		// Modify String Values
		mem, err := strconv.ParseFloat(ud.SystemStats.Mem, 64)
		if err != nil {
			mem = 0
		}
		cpu, err := strconv.ParseFloat(ud.SystemStats.CPU, 64)
		if err != nil {
			cpu = 0
		}
		// Apply Metrics
		c <- prometheus.MustNewConstMetric(s.Internet, prometheus.GaugeValue, float64(internet), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Connected, prometheus.CounterValue, float64(ud.ConnectedAt), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Provisioned, prometheus.CounterValue, float64(ud.ProvisionedAt), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Uplink, prometheus.GaugeValue, float64(uplink), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.LastSeen, prometheus.GaugeValue, float64(ud.LastSeen), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Memory, prometheus.GaugeValue, mem, ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Cpu, prometheus.GaugeValue, cpu, ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Uptime, prometheus.CounterValue, float64(ud.Uptime), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Satisfaction, prometheus.GaugeValue, float64(ud.Satisfaction), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Download, prometheus.CounterValue, float64(ud.RxBytes), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
		c <- prometheus.MustNewConstMetric(s.Upload, prometheus.CounterValue, float64(ud.TxBytes), ud.ID, ud.Name, ud.IP, ud.Model, ud.Type)
	}
}
