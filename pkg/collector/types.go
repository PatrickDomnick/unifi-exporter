package collector

const deviceInternet = "unifi_device_internet"
const deviceConnected = "unifi_device_connected"
const deviceProvisioned = "unifi_device_provisioned"
const deviceUplink = "unifi_device_uplink"
const deviceLastSeen = "unifi_device_last_seen"
const deviceMemory = "unifi_device_memory"
const deviceCpu = "unifi_device_cpu"
const deviceUptime = "unifi_device_uptime"
const deviceSatisfaction = "unifi_device_statisfaction"
const deviceDownload = "unifi_device_download" // RX
const deviceUpload = "unifi_device_upload"     // TX

const clientSatisfaction = "unifi_client_satisfaction"
const clientWired = "unifi_client_wired"
const clientDownload = "unifi_client_download"
const clientUpload = "unifi_client_upload"
const clientUptime = "unifi_device_uptime"
const clientFirstSeen = "unifi_client_first_seen"
const clientLastSeen = "unifi_client_last_seen"
const clientWifiAttempts = "unifi_client_wifi_attempts"

var deviceLabels = []string{"id", "name", "ip", "model", "type"}
var clientLabels = []string{"id", "name", "ip", "oui", "mac"}
