package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/unifi-exporter/pkg/unifi"
)

type UnifiClient struct {
	Satisfaction *prometheus.Desc
	Wired        *prometheus.Desc
	Download     *prometheus.Desc
	Upload       *prometheus.Desc
	Uptime       *prometheus.Desc
	FirstSeen    *prometheus.Desc
	LastSeen     *prometheus.Desc
	WifiAttempts *prometheus.Desc
}

func NewUnifiClient() *UnifiClient {
	return &UnifiClient{
		Satisfaction: prometheus.NewDesc(clientSatisfaction, "Device to Client Satisfaction", clientLabels, nil),
		Wired:        prometheus.NewDesc(clientWired, "Is this client connected via LAN", clientLabels, nil),
		Download:     prometheus.NewDesc(clientDownload, "Total Download in Bytes", clientLabels, nil),
		Upload:       prometheus.NewDesc(clientUpload, "Total Upload in Bytes", clientLabels, nil),
		Uptime:       prometheus.NewDesc(clientUptime, "Uptime in seconds", clientLabels, nil),
		FirstSeen:    prometheus.NewDesc(clientFirstSeen, "When was the device first seen", clientLabels, nil),
		LastSeen:     prometheus.NewDesc(clientLastSeen, "When was the device last seen", clientLabels, nil),
		WifiAttempts: prometheus.NewDesc(clientWifiAttempts, "How many attempts did this client perform to connect to the wifi", clientLabels, nil),
	}
}

func (s *UnifiClient) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *UnifiClient) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for Unifi Devices")
	unifiClients := unifi.GetClients()
	for _, uc := range unifiClients.Data {
		// Modify Boolean Values
		wired := -1
		if uc.IsWired {
			wired = 1
		} else if !uc.IsWired {
			wired = 0
		}
		// Get better Names
		name := uc.Name
		if name == "" {
			name = uc.Hostname
		}
		// Apply Metrics
		c <- prometheus.MustNewConstMetric(s.Satisfaction, prometheus.GaugeValue, float64(uc.Satisfaction), uc.ID, name, uc.IP, uc.Oui, uc.Mac)
		c <- prometheus.MustNewConstMetric(s.Wired, prometheus.GaugeValue, float64(wired), uc.ID, name, uc.IP, uc.Oui, uc.Mac)
		c <- prometheus.MustNewConstMetric(s.Download, prometheus.CounterValue, float64(uc.RxBytes), uc.ID, name, uc.IP, uc.Oui, uc.Mac)
		c <- prometheus.MustNewConstMetric(s.Upload, prometheus.CounterValue, float64(uc.TxBytes), uc.ID, name, uc.IP, uc.Oui, uc.Mac)
		c <- prometheus.MustNewConstMetric(s.FirstSeen, prometheus.CounterValue, float64(uc.FirstSeen), uc.ID, name, uc.IP, uc.Oui, uc.Mac)
		c <- prometheus.MustNewConstMetric(s.LastSeen, prometheus.CounterValue, float64(uc.LastSeen), uc.ID, name, uc.IP, uc.Oui, uc.Mac)
		c <- prometheus.MustNewConstMetric(s.WifiAttempts, prometheus.CounterValue, float64(uc.WifiTxAttempts), uc.ID, name, uc.IP, uc.Oui, uc.Mac)
	}
}
