package unifi

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type unifiClient struct {
	Meta struct {
		Rc string `json:"rc"`
	} `json:"meta"`
	Data []struct {
		SiteID              string      `json:"site_id"`
		AssocTime           int         `json:"assoc_time"`
		LatestAssocTime     int         `json:"latest_assoc_time"`
		Oui                 string      `json:"oui"`
		UserID              string      `json:"user_id"`
		ID                  string      `json:"_id"`
		Mac                 string      `json:"mac"`
		IsGuest             bool        `json:"is_guest"`
		FirstSeen           int         `json:"first_seen"`
		LastSeen            int         `json:"last_seen"`
		IsWired             bool        `json:"is_wired"`
		UsergroupID         string      `json:"usergroup_id"`
		DisconnectTimestamp int         `json:"disconnect_timestamp,omitempty"`
		UptimeByUsw         int         `json:"_uptime_by_usw,omitempty"`
		LastSeenByUsw       int         `json:"_last_seen_by_usw,omitempty"`
		IsGuestByUsw        bool        `json:"_is_guest_by_usw,omitempty"`
		SwMac               string      `json:"sw_mac,omitempty"`
		SwDepth             int         `json:"sw_depth,omitempty"`
		SwPort              int         `json:"sw_port,omitempty"`
		WiredRateMbps       int         `json:"wired_rate_mbps,omitempty"`
		Network             string      `json:"network,omitempty"`
		NetworkID           string      `json:"network_id,omitempty"`
		IP                  string      `json:"ip"`
		Satisfaction        int         `json:"satisfaction"`
		Anomalies           int         `json:"anomalies"`
		Uptime              int         `json:"uptime"`
		WiredTxBytes        int64       `json:"wired-tx_bytes,omitempty"`
		WiredRxBytes        int64       `json:"wired-rx_bytes,omitempty"`
		WiredTxPackets      int         `json:"wired-tx_packets,omitempty"`
		WiredRxPackets      int         `json:"wired-rx_packets,omitempty"`
		WiredTxBytesR       int         `json:"wired-tx_bytes-r,omitempty"`
		WiredRxBytesR       int         `json:"wired-rx_bytes-r,omitempty"`
		Name                string      `json:"name,omitempty"`
		Noted               bool        `json:"noted,omitempty"`
		Hostname            string      `json:"hostname,omitempty"`
		UptimeByUap         int         `json:"_uptime_by_uap,omitempty"`
		LastSeenByUap       int         `json:"_last_seen_by_uap,omitempty"`
		IsGuestByUap        bool        `json:"_is_guest_by_uap,omitempty"`
		ApMac               string      `json:"ap_mac,omitempty"`
		Channel             int         `json:"channel,omitempty"`
		Radio               string      `json:"radio,omitempty"`
		RadioName           string      `json:"radio_name,omitempty"`
		Essid               string      `json:"essid,omitempty"`
		Bssid               string      `json:"bssid,omitempty"`
		PowersaveEnabled    bool        `json:"powersave_enabled,omitempty"`
		Is11R               bool        `json:"is_11r,omitempty"`
		WlanconfID          string      `json:"wlanconf_id,omitempty"`
		UserGroupIDComputed string      `json:"user_group_id_computed,omitempty"`
		Ccq                 int         `json:"ccq,omitempty"`
		Rssi                int         `json:"rssi,omitempty"`
		Noise               int         `json:"noise,omitempty"`
		Signal              int         `json:"signal,omitempty"`
		TxRate              int         `json:"tx_rate,omitempty"`
		RxRate              int         `json:"rx_rate,omitempty"`
		TxPower             int         `json:"tx_power,omitempty"`
		Idletime            int         `json:"idletime,omitempty"`
		DhcpendTime         int         `json:"dhcpend_time,omitempty"`
		AnonClientID        string      `json:"anon_client_id,omitempty"`
		TxMcs               int         `json:"tx_mcs,omitempty"`
		HostnameSource      string      `json:"hostname_source,omitempty"`
		Vlan                int         `json:"vlan,omitempty"`
		RadioProto          string      `json:"radio_proto,omitempty"`
		TxBytes             int         `json:"tx_bytes,omitempty"`
		RxBytes             int         `json:"rx_bytes,omitempty"`
		TxPackets           int         `json:"tx_packets,omitempty"`
		RxPackets           int         `json:"rx_packets,omitempty"`
		BytesR              int         `json:"bytes-r,omitempty"`
		TxBytesR            int         `json:"tx_bytes-r,omitempty"`
		RxBytesR            int         `json:"rx_bytes-r,omitempty"`
		TxRetries           int         `json:"tx_retries,omitempty"`
		WifiTxAttempts      int         `json:"wifi_tx_attempts,omitempty"`
		Authorized          bool        `json:"authorized,omitempty"`
		QosPolicyApplied    bool        `json:"qos_policy_applied,omitempty"`
		EagerlyDiscovered   interface{} `json:"eagerly_discovered,omitempty"`
		RoamCount           int         `json:"roam_count,omitempty"`
	} `json:"data"`
}

func GetClients() unifiClient {
	// Client Endpoint
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", clientUrl))

	clientApiResponse := unifiClient{}

	// Execute the Unifi API Call
	err := getJSON(clientUrl, &clientApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return clientApiResponse
}
