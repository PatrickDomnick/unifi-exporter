package unifi

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

var Client RLHTTPClient

//RLHTTPClient Rate Limited HTTP Client
type RLHTTPClient struct {
	host        string
	csrfToken   string
	unifises    string
	client      *http.Client
	Ratelimiter *rate.Limiter
}

//Do dispatches the HTTP request to the network
func (c *RLHTTPClient) Do(req *http.Request) (*http.Response, error) {
	// Comment out the below 5 lines to turn off ratelimiting
	ctx := context.Background()
	err := c.Ratelimiter.Wait(ctx) // This is a blocking call. Honors the rate limit
	if err != nil {
		return nil, err
	}
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

//NewClient return http client with a ratelimiter
func NewClient(rl *rate.Limiter, host string, username string, password string) error {
	// Create HTTP Client
	client := &http.Client{
		Timeout: time.Second * 10,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	Client = RLHTTPClient{
		host:        host,
		client:      client,
		Ratelimiter: rl,
	}
	// Perform Login
	payload := strings.NewReader(fmt.Sprintf("{\"username\":\"%s\",\"password\":\"%s\"}", username, password))
	url := fmt.Sprintf("%s/%s", host, loginUrl)
	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		log.Error(err)
		return err
	}
	res, err := Client.Do(req)
	if err != nil {
		log.Error(err)
		return err
	}
	log.Info(res.StatusCode)
	cookies := res.Cookies()
	log.Info(cookies)
	for _, cookie := range cookies {
		if cookie.Name == "csrf_token" {
			Client.csrfToken = cookie.Value
		}
		if cookie.Name == "unifises" {
			Client.unifises = cookie.Value
		}
	}
	return nil
}

// Convert JSON to Object
func getJSON(url string, target interface{}) error {
	log.Debug("Preparing Request")
	url = fmt.Sprintf("%s/%s", Client.host, url)
	log.Debug(url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Cookie", fmt.Sprintf("csrf_token=%s;unifises=%s", Client.csrfToken, Client.unifises))

	log.Debug("Executing the Request")
	r, err := Client.Do(req)
	log.Debug(r)
	if err != nil {
		log.Error(err)
		return err
	} else {
		log.Debug("Successfully executed the Request")
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}
